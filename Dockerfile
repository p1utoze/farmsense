# Use the official Python base image
FROM python:3.9

# Set the working directory in the container
WORKDIR /farmsense

# Copy the requirements file to the working directory
COPY requirements.txt .

# Install the Python dependencies
RUN pip install --no-cache-dir -r requirements.txt

# Copy the application code to the container
COPY main.py .

# Copy the .h5 and .json files to the container
# COPY models/model.h5 /farmsense/models
# COPY models/poultry.h5 /farmsense/models
COPY models/seed.h5 models/
# COPY models/predict_data.parquet /farmsense/models
# COPY models/xgbmodel.json models/

# Expose the port on which the FastAPI application will run
EXPOSE 3000

# Start the FastAPI application
# CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "8000"]
CMD ["gunicorn", "main:app", "-w", "4", "-k", "uvicorn.workers.UvicornWorker", "--bind", "0.0.0.0:3000"]
